package com.coe3.browserConfig;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

public class BrowserConfiguration {
	public static Logger log = Logger.getLogger("devpinoyLogger");
	public  WebDriver driver;
	 
    public  WebDriver testLocal(String browserName) {

        try {

                if (browserName.equalsIgnoreCase("chrome")) {

                   
                    log.info("Open Browser -- Chrome");
                   
                    
                  System.setProperty("webdriver.chrome.driver", "C:\\Users\\p005020f\\Downloads\\chromedriver.exe");

                    
                    driver = new ChromeDriver();
                    System.out.println(1);
                   
                } else if (browserName.equalsIgnoreCase("firefox")) {

                  
                    log.info("Open Browser -- Firefox");
                 
                    System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/Drivers/geckodriver");

                    driver = new FirefoxDriver();
                }

            driver.manage().window().maximize();

           
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

       
        } catch (Exception e) {
       e.printStackTrace();
       e.getMessage();
            //log information
            log.error("Not able to open browser -  " + browserName);

        }
       return driver;

    }

    //Function for navigate the URL.
    public void navigate(String URLKey) {

        try {

           
            driver.get(URLKey);

          
            log.info("Navigate to the URL --- " + URLKey);

            
        } catch (Exception e) {

           
            log.error("Not valid URL --- " + URLKey);

            

        }

    }



}
